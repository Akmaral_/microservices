package com.example.brandservice.repo;

import com.example.brandservice.entity.Brand;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BrandRepository extends JpaRepository<Brand, Integer> {


    Brand findByBrandId(Integer id);
}
