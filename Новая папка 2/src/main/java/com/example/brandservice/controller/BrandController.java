package com.example.brandservice.controller;

import com.example.brandservice.VO.ResponseTemplateVO;
import com.example.brandservice.entity.Brand;
import com.example.brandservice.service.BrandService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/brands")
@Slf4j
public class BrandController {
    @Autowired
    private BrandService brandService;

    @PostMapping("/")
    public Brand saveProduct(@RequestBody Brand brand){
        log.info("qwerty");
        return brandService.saveProduct(brand);
    }

    @GetMapping("/{id}")
    public Brand getOneById(@PathVariable Integer id){
        log.info("qwerty");
       return brandService.findBrandById(id);
    }


    @GetMapping("/{id}")
    public ResponseTemplateVO getOne(@PathVariable("id") Integer id) {
        log.info("Inside getUserWithDepartment of UserController");
        return brandService.getOne(id);
    }
}
