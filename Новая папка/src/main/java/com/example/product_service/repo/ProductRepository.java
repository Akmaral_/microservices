package com.example.product_service.repo;

import com.example.product_service.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<Product, Integer> {

    Product findByProductId(Integer id);
}
